# exercise 1
Create a database called dvdrental and import tables and data with the file dvdrental.sql. Leave screenshots of the action!
### code:
```
createdb -U postgres ue1
psql -U postgres ue1 < /path/dvdrental.sql  
```
### Screenshot:
/1.png

+++

# exercise 2
With PL/pgSQL functions the WHILE LOOP statement is supported. Create a function calc_turm. We use an old calculation scheme used in ground schools to learn multiplying and dividing, the so called “Turmrechnung“ or “Zapfenrechnung”. This scheme works with two input variables. The first is the value for the starting number (s_num), the second value defines the height (h_num) of the “Turm”. Both can be issued with values from 1 to 9 


```
For example: s_num = 9; h_num = 5;
9 * 2 = 18
18 * 3 = 54
54 * 4 = 216 
216 * 5 = 1080 
1080 : 2 = 540 
540 : 3 = 180 
180 : 4 = 45 
45 : 5 = 9
```

The function should have two arguments for s_num and h_num and return a two-column TABLE where the first column contains the term as TEXT and the second column contains the result of the term as INTEGER.

Use WHILE LOOP statements while iterating through the calculations. To return the rows of the table you should use the statement RETURN QUERY VALUES (column1, column2) within the loops.

If a value applied is lower than 1 or higher than 9 then raise an exception and leave the function with a RETURN statement.
Show screenshots of the function, selecting the function and its result as well as the exception messages.
+++
### Code:
```
CREATE OR REPLACE FUNCTION calc_turm(s_num integer, h_num integer)
RETURNS TABLE ( calc_text VARCHAR, calc_res VARCHAR )
AS $$
  DECLARE
      i integer := 2;
      up boolean := true;
  BEGIN
    if(s_num > 9 or h_num > 9) then
      raise exception 'no numbers higher than 9';
    end if;


    WHILE i <= h_num
    LOOP
      if (up) then
        calc_text := concat(s_num,'*', i, '=');
        s_num := s_num * i;

        if(i = h_num) then
          up := false;
          i := 1;
        end if;
      else
        calc_text := concat(s_num,':', i, '=');
        s_num := s_num / i;
      end if;

      i := i + 1;
      calc_res := s_num; RETURN NEXT;
    END LOOP;
  END;
$$
LANGUAGE plpgsql;

select * from calc_turm(9,5);
```

### Screenshot:
/2.png

+++

# exercise 3
This task is dedicated to FOR LOOPs. You should create a function that returns actors and their number of roles sorted by either their last names and first names or by the number of roles they played in the available films of DVDRental. Return the function results as a table. Raise an exception, if the input values are not valid.
Screenshots of function, result and for a false input:

### Code:
```
create or replace function actors()
  returns table (actor_name varchar, actor_roles integer)
  as $$
  declare
    rec record;
  begin
    for rec in (select first_name, last_name, count(film_id) as roles from actor join film_actor fa on actor.actor_id = fa.actor_id group by first_name, last_name order by roles desc)
    loop
      actor_name := concat(rec.first_name,' ',rec.last_name); actor_roles := rec.roles; return next;
    end loop;
  end;
$$
  language plpgsql;

select * from actors();
```

### Screenshot:
/3.png

+++
# exercise 4
Create a function called revenue_film(). The function should return a table including the film title and the revenue of each film. Use the screenshot for orientation! Leave Code and screenshot of the result!

### Code
```
create or replace function revenue_film()
  returns table (film_title varchar, film_revenue numeric)
  as $$
  begin
    return query select title, sum(amount) as sum from film join inventory using(film_id) join rental using(inventory_id) join payment using(rental_id) group by title order by sum desc ;
  end;
$$
  language plpgsql;

select * from revenue_film();
```

### Screenshot
/4.png

# exercise 5
Create a function categories (), which has category as an input parameter. The output should be in a string that includes the name of the category and the number of movies in that category.
### Code

```
create or replace function categories(cat varchar)
  returns table (kategorien varchar, count bigint)
  as $$
  begin
      return query select name, count(film_id) as c from category join film_category fc on category.category_id = fc.category_id where name like cat group by name;
  end;
$$
  language plpgsql;


select * from categories('Action');
```

### Screenshot

/5.png

# exercise 6
In this task, you should use CURSOS to change the rental rate of films by selecting a current rental rate and replace all rates by a new value. The function should be called set_rates. The function should do its work in the background and return nothing despite from the notice on a successful operation. Refer to the documentation for PostgreSQL to find out more about the RETURN statement.
### Code
```
create or replace function set_rates(ren_current numeric, ren_new numeric)
  returns void as $$
  declare
    cur_film CURSOR FOR SELECT title, rental_rate FROM film;
  begin
    open cur_film;
    loop
      move next from cur_film;
      exit when not found;
      UPDATE film SET rental_rate = ren_new WHERE rental_rate = ren_current;
    end loop;
    close cur_film;

    raise notice 'done';
  end;
$$
  language plpgsql;


select * from set_rates(4.99,2.99);
```
### Screenshot
/6.png

+++

# exercise 7
The function set_discount is using cursors and should give customers a discount when a certain monetary limit is exceeded. To do this, you should add a column to table “customers” named disct. The column should not be NULL and should have assigned a default value of 0. In a first step, you should design the query to sum the expenses for rental per customer.
### Code
```
alter table customer add column disct integer not null default 0;
query := select first_name, last_name, sum(amount) as sum_pay from customer join payment using(customer_id) group by first_name, last_name;
```

The function will need two arguments, the limit and the discount, both of type NUMERIC. It should return the full name as TEXT and the discount as NUMERIC. To achieve this, use an unbound and a bound cursor.

First you should create a cursor that uses the query you have designed before. Loop through the records and update the column disct with the new discount for the customers exceeding the monetary limit. Nothing needs to be returned in this case.
Next create a cursor that returns a table with the customers which have gotten a discount matching the discount from the provided argument. Step through the rows and return the query values into the right columns of the table.

Code and screenshots of altering the table, the query design plus output, the function, the select statement for the function and the output:
### Code
```
create or replace function set_discount(limit_d numeric, discount numeric)
  returns table (cos integer, disc integer)
  as $$
  declare
    curs cursor for (select * from customer where customer_id = any (select customer_id from customer join payment using(customer_id) group by customer_id having sum(amount) > limit_d));
    cos_id integer;
  begin
    open curs;
    loop
      fetch curs into cos_id;
      exit when not found;
      update customer set disct = discount where customer_id = cos_id;
      cos := cos_id; disc := discount; return next;
    end loop;
    close curs;
    raise notice 'done';
  end;
$$
  language plpgsql;
```

### Screenshot:

/7a.png

/7b.png

/7c.png

+++

# exercise 8
You should use function set_discount for writing a trigger to keep track of changes in terms of discount updates. To achieve it you have to create a trigger function that updates table discount_audit before the change happens. Beside the customer ID and a timestamp, the old discount value should be stored. The function should be named log_discount_change().

Then create a TRIGGER, called discount_change, that fires before an update. To verify the TRIGGER run the same SELECT statement for x as in task 7. Finally write a statement that displays the new records in table discount_audit.
Code and screenshots: create new table, trigger function, trigger, log table

```
CREATE TABLE public.discount_audit
(
    id serial PRIMARY KEY NOT NULL,
    customer_id integer,
    disct numeric,
    changed_on TIMESTAMP
);

CREATE OR REPLACE FUNCTION log_discount_change() RETURNS trigger AS
$BODY$
BEGIN
IF NEW.disct <> OLD.disct THEN
INSERT INTO discount_audit(customer_id,disct,
changed_on) VALUES(OLD.customer_id,OLD.disct,now()); END IF;
RETURN NEW; END;
$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER discount_change BEFORE UPDATE
ON customer
FOR EACH ROW
EXECUTE PROCEDURE log_discount_change();


select * from set_discount(10,10);
```
+++
# exercise 9
As the database has changed create a backup of the DVDRental database and upload it together with the log!

```
pg_dump -U postgres ue1 > /tmp/dvdrental_backup
```

+++
# exercise 10
In the last exercise, you should drop the trigger and the functions you created during the worksheet. Leave the codes!

```
Drop trigger log_discount_change on discount_audit; 
Drop function set_rates(numeric, numeric);
Drop function categories(varchar);
Drop function calc_turm(integer, integer);
```
