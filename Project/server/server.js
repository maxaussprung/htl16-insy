const express = require('express');
const cors = require('cors');

const router = require('./router')

const app = express()

app.use(express.urlencoded({extended: true}))
app.use(express.json())
app.use(cors())

app.use('/movies', router)

app.listen(3000)