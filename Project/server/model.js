const MongoClient = require('mongodb').MongoClient;

let _col

function catchError(cb) {
    return function () {
        try {
            return cb.apply(this, arguments);
        } catch (error) {
            throw new Error('connect to mongo first')
        }
    }
}

function connect(options) {
    MongoClient.connect(options.url, {
        useNewUrlParser: true
    }, (err, client) => {
        if (err) throw new Error(err)
        _col = client.db(options.db).collection(options.collection)
        console.log('mongo connected')
    });
}

function find(query = {}) {
    return _col.find(query).toArray()
}

function insert(items) {
    return _col.insertOne(items)
}

function update(query, object) {
    return _col.updateOne(query, { $set:object})
}

function remove(query) {
    return _col.deleteOne(query)
}


module.exports = {
    connect,
    find: catchError(find),
    insert: catchError(insert),
    update: catchError(update),
    remove: catchError(remove)
}
