const router = require('express').Router()
const mongo = require('./model')

const {
    ObjectId
} = require('mongodb')

mongo.connect({
    url: 'mongodb://localhost:27017',
    db: 'insy',
    collection: 'movies'
})

router.get('/', async (req, res) => {
    res.send(await mongo.find())
})

router.get('/:id', async (req, res) => {
    res.send(await mongo.find({'_id': ObjectId(req.params.id)}))
})

router.post('/', async (req, res) => {
    console.log(req.body)
    res.send(await mongo.insert(req.body.data))
})

router.put('/:id', async (req, res) => {
    res.send(await mongo.update({'_id': ObjectId(req.params.id)}, req.body.data))
})

router.delete('/:id', async (req, res) => {
    res.send(await mongo.remove({
        '_id': ObjectId(req.params.id)
    }))
})

module.exports = router;