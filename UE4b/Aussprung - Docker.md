# Docker I
## Installing Docker on MacOS

	brew cask install docker
	
## Running the first container

Please type the following command into your terminal window and hit return.
	
	docker run alpine echo "Hello HTL Ottakring"

![](1.png)

+++

Let's try to run the very same command again:
	
![](2.png)

Try to reason about why the first time you run a command you see a different output than all the subsequent times.

**Docker doesn't need to download the image, because it already did**
 
## Starting, stopping, and removing containers

	docker images

![](3.png)

+++

	docker ps
 
![](4.png)

To list all the containers (running and stopped)

	docker ps -a
	
![](5.png)

+++

Let's run another container. This time we give it the name pingonly.
	
	docker run --name pingonly centos ping -c 4 127.0.0.1
	
![](6.png)

We can start a container by using the command start:

	docker start pingonly
	
![](7.png)	

+++

Obiously the container did start, but where's the output? Turns out, that when a container is started there is no terminal
associated with it. So we do not see the output from ping (a terminal was attached automotically when we typed run).We
can use the -a switch to attach a terminal for output:

	docker start pingonly -a

![](8.png)	

Congratulations! You just used an entire OS to build an inflated ping!


Now, start the Alpine container!

![](9.png)	

+++

A container can be removed (if they are stopped, else use first the stop/kill command!) by using the rm command.

	docker rm pingonly; docker ps -a; docker images

![](10.png)	

+++

## Using a container interactively

We can create a new container an start a bash shell to work interactively with the centOS by using the `-it` switch. Type:

	docker run --name centi -it centos

![](11.png)	

+++

To work with the container interactively we need to attach a terminal by using the `-i` switch:
	
	docker start centi -i

![](12.png)	

	yum install nano

![](13.png)	

+++

	nano ~/hewo.txt
	
![](14.png)	

	docker cp centi:/root/hewo.txt Desktop/hewo.txt && nano Desktop/hewo.txt 
	
![](15.png)

Copy the file back into the container and view it with nano!

![](16.png)
![](17.png)

# Docker 2

Next, we will pull the latest release of PostgreSQL from the docker hub and create a container. Some points to consider:

* We would like to access the database from outside the container (for example via DataGrip). So, we map the port of PostgreSQL in the container to a port of the host OS (with the -p switch).
* PostgreSQL needs a password for the superuser postgres. We will set this password with the environment variable POSTGRES_PASSWORD.
* We also would like to run the database as a service (demon in Unix lingo). For this we use the -d switch. Type the following in a PowerShell console:

```
docker run --name mypostgres -e POSTGRES_PASSWORD=postgres -d -p 5433:5432 postgres
```

![](18.png)

With DataGrip, create a database demo in the container PostgreSQL.

![](19.png)

Using the shell, create in database demo a table test with a few data records.

```
create table test
(
	id serial
		constraint test_pk
			primary key,
	name varchar(30)
);

INSERT INTO "public"."test" ("id", "name") VALUES (DEFAULT, 'BigDino')

INSERT INTO "public"."test" ("id", "name") VALUES (DEFAULT, 'Pizza')

INSERT INTO "public"."test" ("id", "name") VALUES (DEFAULT, 'pink ball')

INSERT INTO "public"."test" ("id", "name") VALUES (DEFAULT, 'yobi')
```

Use pg_dump to dump the database.

	pg_dump -U postgres  demo > ~/demo.sql


Copy the file from the container to your host OS. Examine it with an editor.

```
docker cp centi:/root/demo.sql Desktop/demo.sql && nano Desktop/demo.sql
```

![](20.png)

## Docker Compose
### YAML

Dictionaries can be nested in lists (and vice versa) to create more complex structures:

	user:
	- Willy Wonka
	- address:
		- Thaliastraße 125
		- Vienna

+++

Convert it to JSON!

	{
	   "user": [
	      "Willy Wonka",
	      {
	         "address": [
	            "Thaliastraße 125",
	            "Vienna"
	         ]
	      }
	   ]
	}

Let's create and start the containers

```
version: '3.7'
services:
  db:
    container_name: mypostgres
    restart: unless-stopped
    image: postgres
    ports:
      - '5433:5432' 
	  environment:
      POSTGRES_USER: postgres 
	    POSTGRES_PASSWORD: postgres
      PGDATA: /data/postgres
  pgadmin:
    container_name: mypgadmin
    image: dpage/pgadmin4
    environment:
      PGADMIN_DEFAULT_EMAIL: pgadmin@mail.com
      PGADMIN_DEFAULT_PASSWORD: pgadmin
    ports:
      - "8080:80"
    restart: unless-stopped    
```

	> docker-compose up

![](21.png)

Surfing with our browser to port 8080 we see the web interface of pgadmin4!

Create a table and fill it with some data.

![](23.png)

Open DataGrip and verify the table!

![](24.png)

+++

### Using MongoDB with Docker
<br>

```
version: '3.7'
services:
  mongo:
    container_name: mymongo
    restart: always
    image: mongo
    ports:
    - '27017:27017'
    environment:
      MONGO_INITDB_ROOT_USERNAME: admin
      MONGO_INITDB_ROOT_PASSWORD: admin
    command:
      mongod --auth
```

	> docker-compose up

![](25.png)

+++

connection string 

	mongodb://admin:admin@127.0.0.1:27017

![](26.png)

