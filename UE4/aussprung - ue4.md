# Aufgabe 1
Obwohl PostgreSQL das Isolation Level „Uncommited Read“ nicht unterstützt, wollen wir trotzdem einen Versuch wagen und überprüfen, ob im Standard Isolation Level ein „Dirty Read“ möglich ist. Eigentlich sollte eine Transaktion mit obiger Einstellung folgendermaßen ablaufen:

Öffne zwei Shells und verwende die Datenbank „blumenhandel“. Setze ein einfaches SELECT- Statement über die Tabelle „artikel“ in beiden Shells ab. (Screenshot) 

	Select * from artikel

/1a.png

Starte in der ersten Shell eine Transaktion (T1). Verringere den Bestand der Tulpen um 1000 Stück! In der zweiten Shell starte ebenfalls eine Transaktion (T2) und setze ein SELECT- Statement auf den Bestand der Tulpen ab. Anschließend beende T1 mit einem ROLLBACK und T2 mit einem COMMIT. (Screenshot)

```
T1:

Begin;
update artikel set bestand = 11000 where a_nr = 2;
Rollback;

T2:
Select * from artikel;
```

/1b.png

Wie bereits weiter oben erwähnt, verwendet PostgreSQL „Read Commited“ als Standardeinstellung als Isolation Level. Im nächsten Schritt soll das Isolation Level der Transaktionen auf „Read Uncommited“ gesetzt. Wiederhole obige Anweisungen und Einerseits ignoriert PostgreSQL das Isolation Level „Read Uncommited“ und behandelt die Transaktion als „Read Commited“, andererseits findet ein Prozess statt, der als Concurency Control oder Nebenläufigkeitskontrolle bezeichnet wird.

```

T1:
begin transaction isolation level read uncommitted;
update artikel set bestand = 11000 where a_nr = 2;
Rollback;

T2:
Select * from artikel;
```

/1c.png

PostgreSQL verwendet ein Verfahren, das als Multiversion Concurrency Control (MVCC) bezeichnet wird. Konkurrierende Zugriffe auf eine Datenbank werden dabei möglichst effizient ausgeführt, ohne Datensätze zu blockieren oder die Konsistenz der Daten zu gefährden. Das Verfahren verwendet Zeitstempel für Read/Write Aktionen während einer Transaktion, merkt sich also den Zustand der Datenbank vor den konkurrierenden Transaktionen. Der Nachteil dieses Systems sind die Kosten, um viele verschiedene Versionen eines Objekts in der Datenbank zu halten. Auf der anderen Seite werden lesende Zugriffe nie blockiert, was sehr wichtig bei ausgelasteten Datenbanken sein kann, in denen sehr häufig nur gelesen wird.

Nachzulesen unter [https://de.wikipedia.org/wiki/Multiversion_Concurrency_Control](https://de.wikipedia.org/wiki/Multiversion_Concurrency_Control)
Suche nach den Begriffen „Transaktionaler Speicher“ und „Zeitstempelverfahren“ und beschreibe deren zu Grunde liegenden Konzepte!

### Transnationaler Speicher
*Bei der parallelen Programmierung können sich schnell Fehler einschleichen, welche zu Blockierungen innerhalb des Systems (so genannte Deadlocks) führen können. Transaktionaler Speicher soll dieses Problem grundsätzlich lösen und dadurch die Parallelisierung von Software erheblich vereinfachen. Die Idee des transaktionalen Speichers stammt aus der Architektur von Datenbanksystemen. Eine Transaktion bündelt Befehle, die auf gemeinsame Ressourcen zugreifen. Falls zwei Transaktionen auf die gleiche Ressource zugreifen möchten, wird eine der beiden Transaktionen abgebrochen. Diese gibt die besetzten Ressourcen frei und setzt alle getätigten Änderungen zurück (Rollback). Welche Transaktion abgebrochen wird, entscheidet ein intelligenter Verwaltungsmechanismus, das Herzstück eines transaktionalen Speichers.*

[https://de.wikipedia.org/wiki/Transaktionaler_Speicher](https://de.wikipedia.org/wiki/Transaktionaler_Speicher)

### Zeitstempelverfahren
*itstempelverfahren (englisch timestamp ordering) werden in Datenbanksystemen eingesetzt, um die Forderung der Isolation des ACID-Prinzips bei Transaktionen zu erfüllen.Zeitstempelverfahren fallen in die Kategorie der optimistischen Synchronisationsverfahren, das heißt, es wird bei dem Start einer Transaktion davon ausgegangen, dass die Wahrscheinlichkeit eines Konflikts mit einer parallel ablaufenden Transaktion (TA) niedrig ist. Transaktionen werden nur dann zurückgesetzt, wenn es zu einem Konflikt gekommen ist, oder gekommen sein könnte. Um die Konflikte erkennen zu können, bekommen Transaktionen und auch Objekte Zeitstempel. Nun kann vor jedem Lesebeziehungsweise Schreibzugriff auf ein Objekt geprüft werden, ob das Objekt bereits von einer parallel laufenden TA geändert worden ist, also ein Konflikt entstanden ist, andernfalls kann der Zeitstempel aktualisiert werden. In praktischen Anwendungen findet nicht vor jedem Zugriff auf ein Objekt eine Validierung statt, sondern die Änderungen einer Transaktion befinden sich vorerst in einem Puffer, so dass das tatsächliche Einbringen in die Datenbank (Schreiben auf Festplatte) erst nach erfolgreicher Validierungsphase (vor dem Ausführen des Commits, des Schreibvorgangs) durchgeführt wird.*

[https://de.wikipedia.org/wiki/Zeitstempelverfahren](https://de.wikipedia.org/wiki/Zeitstempelverfahren)
 
# Aufgabe 2
Eine der Anomalien wird als „Lost Update/Dirty Write“ bezeichnet.
In einer Transaktion T1 verringere den Bestand der Rosen um 1000 Stück, in einer Transaktion T2 die Anzahl der Rosen um 2500 Stück. Anschließend beende Transaktion 1 mit COMMIT und auch Transaktion 2.

Zeige mit einem SELECT den neuen Rosenbestand! Was passiert nach dem abgesetzten UPDATE der Transaktion T2 und dem COMMIT in Transaktion T1? Begründe deine Antwort und hinterlasse Screenshots.

```
T1:
begin transaction isolation level read uncommitted; //2
update artikel set bestand = bestand - 1000 where a_nr = 2; //3
commit; //6
Select * from artikel; //8

T2:
Select * from artikel; //1
begin transaction isolation level read uncommitted; //4
update artikel set bestand = bestand - 2500 where a_nr = 2; //5
commit; //7
```

/2.png

**Antwort:**
Theoretisch sollte der commit in t2 ignoriert werden da er von t1 commit überschrieben wird. Ich konnte diese Anomalie jedoch nicht hervorrufen. Hab es mit einer red committed und read uncommitted versucht. Da wie schon oben beschrieben uncommitted *keinen* deutlichen unterschied zu committed macht, haben sie auch das selbe getan. Sobald ich eine Transaktion gestartet habe, währenddessen eine zweite eingeben habe und mit commit bestätigt habe, ist nichts passiert. Erst nachdem ich mit commit die erste Transaktion bestätigt habe, wurde schließlich auch die 2te ausgeführt, jedoch danach.

# Aufgabe 3
Bei der nächsten Aufgabe wollen wir die „non-repeatable read“ Anomalie untersuchen.

Setze nun in einer Transaktion T1 ein SELECT Statement auf den Bestand der Rosen ab. In einer Transaktion T2 erhöhe den Bestand der Rosen um 3500 Stück. Zeige beide Transaktionen in Screenshots in zeitlicher Reihenfolge der jeweiligen Aktionen. Vergiss nicht Transaktion 1 mit einem COMMIT abzuschließen!
Konnte der „non-repeatable read“ erfolgreich durchgeführt werden?

**Ja**

Welches Isolation Level müsste eingesetzt werden um diese Anomalie zu unterdrücken?

**Repeatable Read**

```
T1:
Begin; //1
select bestand from artikel where a_nr = 1; //4
select bestand from artikel where a_nr = 1; //6


T2:
begin; //2
update artikel set bestand = bestand + 3500 where a_nr = 1; //3
Commit; //5
```

/3.png

# Aufgabe 4
Spiele Aufgabe 3 nun im Isolation Level REPEATABLE READ durch. Hinterlasse Screenshots der beiden Transaktionen und beschreibe, was passiert! Ist dieses Verhalten immer wünschenswert?

**Transaktion ist isoliert und hat einen Snapshot vom Anfang der  Transaktion, dh die Werte verändern sich bei einem read nicht**


/4.png
