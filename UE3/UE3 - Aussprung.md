# Aufgabe 1

Sieh dir Einstellungen zum WRITE AHEAD LOG in der ..\PostgreSQL\9.6\data\postgresql.conf an. Du findest einige Parameter, mit welchen das Verhalten beim Schreiben/Lesen des Logs eingestellt werden können. Die Dokumentation von PostgrSQL listet die verschiedenen Einstellungsmöglichkeiten auf. Sieh dir die Einstellungsmöglichkeiten unter

[https://www.postgresql.org/docs/9.6/static/runtime-config-wal.html](https://www.postgresql.org/docs/9.6/static/runtime-config-wal.html) 

an und beschreiben in ein, zwei kurzen Sätzen die Funktion folgender Parameter:
synchronous_commit, full_page_writes, wal_buffers, wal_writer_delay.


```
#synchronous_commit = on        # synchronization level;
                               
#full_page_writes = on          # recover from partial 

#wal_buffers = -1               # min 32kB, -1 sets 

#wal_writer_delay = 200ms       # 1-10000 milliseconds
```

## Answer:
**synchronous_commit** <br>
Asynchronous commit is an option that allows transactions to complete more quickly, at the cost that the most recent transactions may be lost if the database should crash. In many applications this is an acceptable trade-off.

**full_page_writes (boolean)** <br>
When this parameter is on, the PostgreSQL server writes the entire content of each disk page to WAL during the first modification of that page after a checkpoint. This is needed because a page write that is in process during an operating system crash might be only partially completed, leading to an on-disk page that contains a mix of old and new data. The row-level change data normally stored in WAL will not be enough to completely restore such a page during post-crash recovery. Storing the full page image guarantees that the page can be correctly restored, but at the price of increasing the amount of data that must be written to WAL. 

**wal_buffers (integer)** <br>
The amount of shared memory used for WAL data that has not yet been written to disk. The default setting of -1 selects a size equal to 1/32nd (about 3%) of shared_buffers, but not less than 64kB nor more than the size of one WAL segment, typically 16MB. This value can be set manually if the automatic choice is too large or too small, but any positive value less than 32kB will be treated as 32kB. This parameter can only be set at server start.The contents of the WAL buffers are written out to disk at every transaction commit, so extremely large values are unlikely to provide a significant benefit. However, setting this value to at least a few megabytes can improve write performance on a busy server where many clients are committing at once. The auto-tuning selected by the default setting of -1 should give reasonable results in most cases.

**wal_writer_delay (integer)** <br>
Specifies the delay between activity rounds for the WAL writer. In each round the writer will flush WAL to disk. It then sleeps for wal_writer_delay milliseconds, and repeats. The default value is 200 milliseconds (200ms). Note that on many systems, the effective resolution of sleep delays is 10 milliseconds; setting wal_writer_delay to a value that is not a multiple of 10 might have the same results as setting it to the next higher multiple of 10. This parameter can only be set in the postgresql.conf file or on the server command line.

# Aufgabe 2
Erstelle eine Datenbank mit dem Namen „laden“ mit einer Tabelle „mitarbeiter“, die folgende Attribute beinhalten soll:

```
id ... SERIAL PRIMARY KEY 
vorname ... VARCHAR(30) 
nachname ... VARCHAR(30) 
alter ... INTEGER
stadt ... VARCHAR(30) 
gehalt ... NUMERIC
```
+++
### Code:
```
create database laden;

create table public.mitarbeiter
(
    id serial primary key,
    vorname varchar(30),
    nachname varchar(30),
    alter integer,
    stadt varchar(30),
    gehalt numeric
);
```

### Screenshot:
/2.png

+++

# Aufgabe 3
In einem ersten Schritt wollen wir die Atomizität einer Transaktion überprüfen. Öffne eine PostgreSQL Command Shell. Probiere folgendes aus und verwende dazu deinen Vor- und Nachnamen und den deines Nachbarn. Beachte, dass im ersten INSERT-Statement die Hochkommas für die Eingabe von Strings und Datum fehlen müssen!

```
BEGIN;

INSERT INTO mitarbeiter (vorname, nachname, geburtsdatum, gehalt) VALUES (<dein_vorname>, <dein_nachname>, <dein_geburtsdatum>, 2307.3);

INTO mitarbeiter (vorname, nachname, geburtsdatum, gehalt) VALUES ('<sein_vorname>', '<sein_nachname>', '<sein_geburtsdatum>', 2306.3);

COMMIT;
```

### Code 

``` 
//Fix
ALTER TABLE public.mitarbeiter ADD geburtsdatum varchar(30) NULL;
```

```
BEGIN;

INSERT INTO mitarbeiter (vorname, nachname, geburtsdatum, gehalt) VALUES ('Max','Aussprung','2000-05-30', 2307.3);

INSERT INTO mitarbeiter (vorname, nachname, geburtsdatum, gehalt) VALUES ('Max', 'Kuchler', '1999-10-06', 2306.3);

COMMIT;
```

### Screenshot
/3.png

*Hinweis: Die Transaktion wird in diesem Beispiel erfolgreich gestartet. Nach der Abarbeitung eines fehlerhaften Statements werden folgende korrekte Anweisungen vom System ignoriert. Mit einem COMMIT wird wegen der Atomizitätskriterien einer Transaktion ein ROLLBACK angestoßen.*
+++
# Aufgabe 4
Trage nun folgende Daten in die Tabelle ein!

```
INSERT INTO mitarbeiter (vorname, nachname, geburtsdatum, gehalt) VALUES
('Herbert','Frasel','1993-04-20',1924.7), ('Alfred','Gans','1989-09-12',2419.2), ('Astrid','Schwarz','1998-02-06',1762.9), ('Albert','Brugger','1987-12-21',1643.7), ('Georg','Büchner','1997-05-18',1987.4), ('Ronald','Schuh','1982-01-01',2924.7);
```

Zeige das INSERT-Statement und die befüllte Tabelle in einem Screenshot!
### Screenshot
/4.png
+++
# Aufgabe 5

Mittels einer Transaktion lösche nun alle Mitarbeiter, die unter 25 Jahre alt sind. Danach lasse dir mit einem SELECT Statement die Tabelle mit dem erfolgreichen Ergebnis der Transaktion auszugeben. Hinterlasse das Transaktions-Statement als Text und einen Screenshot der Ausgabe!

Um das Alter zu errechnen, benötigst du folgende Funktion:

	DATE_PART('year', [DATE] enddate) - DATE_PART('year', [DATE] startdate)

### Code:
```
Begin;

delete from mitarbeiter where (date_part('year', now()) - date_part('year',  date(geburtsdatum) )) < 25;

Commit;
```

/5.png
+++
# Aufgabe 6
Füge nun die gelöschten Datensätze wieder hinzu!

```
INSERT INTO mitarbeiter (vorname, nachname, geburtsdatum, gehalt) VALUES
('Astrid','Schwarz','1998-02-06',1762.9), ('Georg','Büchner','1997-05-18',1987.4);
```

Verwende ein weiteres Mal die Transaktion aus Aufgabe 5, sieh dir aber vor dem COMMIT Statement den Inhalt der Tabelle in einer zweiten PostgreSQL-Konsole an. Hinterlasse einen Screenshot in Reihenfolge deiner Aktionen!
Vergiss nicht, die laufende Transaktion mit einem COMMIT abzuschließen!

### Screenshot
/6.png
+++
# Aufgabe 7
Mit dem Kommando ROLLBACK kann man eine Transaktion zurückrollen, da die Daten noch nicht physisch gespeichert wurden. Füge dazu die beiden gelöschten Datensätze wieder ein! Anstatt des Kommandos COMMIT verwende nun das Kommando ROLLBACK. Hinterlasse die Statements in Textform und einen Screenshot des SELECT Statements vor und im Anschluss an das erfolgreiche Rollback!

/7.png
+++
# Aufgabe 8
Innerhalb einer Transaktion kann man sogenannte SAVEPOINTs setzen um eine Transaktion bis zu einem bestimmten Zeitpunkt rückzusetzen. Die Syntax dazu sieht folgendermaßen aus:

|-|-|
|SAVEPOINT < savepoint_name >|Erstellt einen SAVEPOINT|
|ROLLBACK TO < savepoint_name >|ROLLBACK bis zu einem SAVEPOINT. Alle Aktionen bis zu diesem SAVEPOINT werden rückgängig gemacht.|
|RELEASE SAVEPOINT < savepoint_name >|Das RELEASE Statement wird genutzt um einen SAVEPOINT zu deaktivieren.|

Du willst nun die Datensätze mit den IDs 1 – 4 löschen. Du bist dir aber noch nicht sicher, ob du das wirklich tun willst. Setze vor jedem DELETE je einen SAVEPOINT mit den Namen SP1 bis SP4. Setze nun ein ROLLBACK zum SAVEPOINT SP2. Hinterlasse die Transaktion in Textform und hinterlasse einen Screenshot der Tabelle nach der Transaktion!

*Hinweis: das COMMIT für den Abschluss der Transaktion nicht vergessen!*
### Code:
```
select * from mitarbeiter;

Begin;

SAVEPOINT SP1;
delete from mitarbeiter where id = 1;
SAVEPOINT SP2;
delete from mitarbeiter where id = 2;
SAVEPOINT SP3;
delete from mitarbeiter where id = 3;
SAVEPOINT SP4;
delete from mitarbeiter where id = 4;

rollback to SP2;

select * from mitarbeiter;

commit;
```

/8.png
 

